package net.proloquor.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UuidUtilTest {

    static final String uuidString = "65942d74-eb9e-4872-8c0f-a742cdf97e7b";
    static final String base64String = "ckie63QtlGV7fvnNQqcPjA==";

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void uuidToBase64() {
        assertEquals(base64String, UuidUtil.uuidToBase64(UUID.fromString(uuidString)));
    }

    @Test
    void base64ToUuid() {
        assertEquals(uuidString, UuidUtil.base64ToUuid(base64String).toString());
    }

    @Test
    void testBadBase64String() {
        assertThrows(IllegalArgumentException.class, () ->
                UuidUtil.base64ToUuid("Bad Base64 String"));
    }
}