package net.proloquor.pdl;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PDLStringListenerTest {

    String sample = "0bd1b64b-a4f0-4605-a921-be1a86284cba : 1";

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testEnterPdl() {
        PDLLexer lexer = new PDLLexer(CharStreams.fromString(sample));
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        PDLParser parser = new PDLParser(tokens);
        ParseTreeWalker walker = new ParseTreeWalker();
        PDLStringListener listener = new PDLStringListener();
        walker.walk(listener, parser.pdl());
    }
}