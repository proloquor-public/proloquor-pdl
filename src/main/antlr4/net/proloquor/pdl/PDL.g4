grammar PDL;
options {caseInsensitive = true;}

pdl : questionExp EOF;

questionExp : question ':' answerExp
            | '(' questionExp ')'
            | NOT questionExp
            | questionExp AND questionExp
            | questionExp OR questionExp ;

question : questionId 
         | memberId '@' questionId ; 

memberId : UUID;
questionId : UUID;

answerExp : mcAnswerExp 
          | mrAnswerExp
          | ffAnswerExp;

mcAnswerExp : INDEX
            | '(' mcAnswerExp ')'
            | NOT mcAnswerExp 
            | mcAnswerExp AND mcAnswerExp 
            | mcAnswerExp OR mcAnswerExp ;

ffAnswerExp : REGEX;
mrAnswerExp : '[' INDEX (',' INDEX)* ']'
            | NOT mrAnswerExp
            | mrAnswerExp OR mrAnswerExp ;

fragment HEX : [0-9a-f];
fragment HEX4 : HEX HEX HEX HEX;
fragment HEX8 : HEX4 HEX4;
fragment HEX12 : HEX4 HEX8;

OR : 'or';
AND : 'and';
NOT : 'not';

REGEX : '"' .+?'"';
UUID : HEX8 '-' HEX4 '-' HEX4 '-' HEX4 '-' HEX12;
INDEX : [1-9][0-9]*;

WS : [ \t\n\r]+ -> skip;