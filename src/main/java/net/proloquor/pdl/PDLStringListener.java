package net.proloquor.pdl;

import org.antlr.v4.runtime.ParserRuleContext;

public class PDLStringListener extends PDLBaseListener {
    @Override
    public void enterPdl(PDLParser.PdlContext ctx) {
        super.enterPdl(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitPdl(PDLParser.PdlContext ctx) {
        super.exitPdl(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterQuestionExp(PDLParser.QuestionExpContext ctx) {
        super.enterQuestionExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitQuestionExp(PDLParser.QuestionExpContext ctx) {
        super.exitQuestionExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterQuestion(PDLParser.QuestionContext ctx) {
        super.enterQuestion(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitQuestion(PDLParser.QuestionContext ctx) {
        super.exitQuestion(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterMemberId(PDLParser.MemberIdContext ctx) {
        super.enterMemberId(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitMemberId(PDLParser.MemberIdContext ctx) {
        super.exitMemberId(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterQuestionId(PDLParser.QuestionIdContext ctx) {
        super.enterQuestionId(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitQuestionId(PDLParser.QuestionIdContext ctx) {
        super.exitQuestionId(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterAnswerExp(PDLParser.AnswerExpContext ctx) {
        super.enterAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitAnswerExp(PDLParser.AnswerExpContext ctx) {
        super.exitAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterMcAnswerExp(PDLParser.McAnswerExpContext ctx) {
        super.enterMcAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitMcAnswerExp(PDLParser.McAnswerExpContext ctx) {
        super.exitMcAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterFfAnswerExp(PDLParser.FfAnswerExpContext ctx) {
        super.enterFfAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitFfAnswerExp(PDLParser.FfAnswerExpContext ctx) {
        super.exitFfAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterMrAnswerExp(PDLParser.MrAnswerExpContext ctx) {
        super.enterMrAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitMrAnswerExp(PDLParser.MrAnswerExpContext ctx) {
        super.exitMrAnswerExp(ctx);
        String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void enterEveryRule(ParserRuleContext ctx) {
        super.enterEveryRule(ctx);
        // String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        // System.out.printf("%s: %s%n", methodName, ctx.getText());
    }

    @Override
    public void exitEveryRule(ParserRuleContext ctx) {
        super.exitEveryRule(ctx);
        // String methodName = new Object() {}.getClass().getEnclosingMethod().getName();
        // System.out.printf("%s: %s%n", methodName, ctx.getText());
    }
}
