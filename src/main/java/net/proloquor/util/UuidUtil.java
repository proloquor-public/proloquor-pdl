package net.proloquor.util;

import java.nio.ByteBuffer;
import java.util.Base64;
import java.util.UUID;

public class UuidUtil {

    public static boolean swap = true;

    public static String uuidToBase64(UUID uuid) {
        byte[] bytes = ByteBuffer.wrap(new byte[16])
                .putLong(swapLong(uuid.getMostSignificantBits()))
                .putLong(swapLong(uuid.getLeastSignificantBits()))
                .array();

        return Base64.getEncoder().encodeToString(bytes);
    }

    public static UUID base64ToUuid(String base64) {
        byte[] bytes2 = Base64.getDecoder().decode(base64);
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes2);
        long high = swapLong(byteBuffer.getLong());
        long low = swapLong(byteBuffer.getLong());

        return new UUID(high, low);
    }

    private static long swapLong (long value)
    {
        if(!swap) {
            return value;
        }

        long b1 = (value) & 0xff;
        long b2 = (value >>  8) & 0xff;
        long b3 = (value >> 16) & 0xff;
        long b4 = (value >> 24) & 0xff;
        long b5 = (value >> 32) & 0xff;
        long b6 = (value >> 40) & 0xff;
        long b7 = (value >> 48) & 0xff;
        long b8 = (value >> 56) & 0xff;

        return b1 << 56 | b2 << 48 | b3 << 40 | b4 << 32 |
                b5 << 24 | b6 << 16 | b7 <<  8 | b8;
    }
}
